import React from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';
import './style.css';

const About = () => {
    return (
        <div className="container">
            <h5 className="mt-3">Home / About</h5>
            <ListGroup>
                <ListGroupItem>Gustavo dos Santos Sobrinho</ListGroupItem>
                <ListGroupItem>Raphaela Paolla dos Santos</ListGroupItem>
                <ListGroupItem>João Victor Sponchiado</ListGroupItem>
                <ListGroupItem>Fagner Octávio</ListGroupItem>
                <ListGroupItem>Natal Júnior</ListGroupItem>
            </ListGroup>
        </div>
    )
}

export default About;