import React from 'react';
import { Link } from 'react-router-dom';

import './style.css';



const Error = () => {
    return (
        <div className="container center">
            <h3 className="mt-3">Ops... Esta página não existe!</h3>
            <Link to="/" className="btn">Voltar à página inicial!</Link>
        </div>
    )
}

export default Error;