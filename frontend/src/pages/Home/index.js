import React, { useState } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
  Card, 
  CardImg,
  CardBody,
  CardTitle
} from 'reactstrap';

import './style.css';
import saude from './saude.jpg';
import saude2 from './saude2.jpg';
import saude3 from './saude3.jpg';
import medicamento from './medicamento1.jpg';
import medicamento2 from './medicamento2.jpg';

// Items Carousel
const items = [
    {
      src: saude,
      altText: 'Primeiro Slide'
    },

    {
      src: saude2,
      altText: 'Segundo Slide'
    },

    {
      src: saude3,
      altText: 'Terceiro Slide'
    }
];

const Home = () => {

    // Carousel
    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);

    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
      }
    
      const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
      }
    
      const goToIndex = (newIndex) => {
        if (animating) return;
        setActiveIndex(newIndex);
      }

      const slides = items.map((item) => {
        return (
          <CarouselItem
            onExiting={() => setAnimating(true)}
            onExited={() => setAnimating(false)}
            key={item.src}
          >
            <img src={item.src} alt={item.altText} width="100%"/>
            <CarouselCaption captionText={item.caption} captionHeader={item.caption} />
          </CarouselItem>
        );
      });

      return (
        <div className="container">

            {/* Carousel */}
            <section>
                <Carousel activeIndex={activeIndex} next={next} previous={previous}>
                <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
                {slides}
                <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
                <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
                </Carousel>
            </section>

            {/* Cards */}
            <section>
              <div className="container mt-5">
                <span className="categorias">Alguns Medicamentos</span>
                <div className="row mt-3">
                  <div className="col-sm-3">
                    <Card>
                      <CardImg top width="100%" src={medicamento} alt="Aspirinas" />
                      <CardBody>
                        <CardTitle>Aspirinas</CardTitle>
                      </CardBody>
                    </Card>
                  </div>

                  <div className="col-sm-3">
                    <Card>
                      <CardImg top width="100%" src={medicamento} alt="Analgesicos" />
                      <CardBody>
                        <CardTitle>Analgésicos</CardTitle>
                      </CardBody>
                    </Card>
                  </div>

                  <div className="col-sm-3">
                    <Card>
                      <CardImg top width="100%" src={medicamento2} alt="Azitromicina" />
                      <CardBody>
                        <CardTitle>Azitromicina</CardTitle>
                      </CardBody>
                    </Card>
                  </div>

                  <div className="col-sm-3">
                    <Card>
                      <CardImg top width="100%" src={medicamento2} alt="Card image cap" />
                      <CardBody>
                        <CardTitle>Azitromicina</CardTitle>
                      </CardBody>
                    </Card>
                  </div>
                </div>
              </div>
            </section>
        </div>
      );
}

export default Home;